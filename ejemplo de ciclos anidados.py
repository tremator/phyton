mensajedeentrada="programa de generacion de tablas de multiplicar".capitalize()#el capitalize sirve parqa que la primera letra salga mayuscula
print(mensajedeentrada.center(60,"="))#ejemplo para centrar el mensaje,rellenando los espacios que quedan a los lados


#opcion=1
opcion="s"
#este siver para salir con numeros
#while opcion!=0:
#este sirve para salir pero con letras
while opcion!="n" and opcion!="N":#esta es la condicionante para salir
    tabinicio=int(input(chr(27)+"[1;35m"+"ingrese la tabla inicial: "))
    tabfinal=int(input("ingrese la tabla final: "))

    while tabfinal<tabinicio:#este le indica al usuario que la tabla final debe ser menor que la inicial
        print("la tabla de incio debe de ser menor que la tabla final: ")
        tabinicio=int(input("ingrese la tabla inicial: "))
        tabfinal=int(input("ingrese la tabla final: "))


    rangoinicio=int(input("ingrese el rango inicial a multiplicar: "))
    rangofinal=int(input("ingrese el rango final: "))

    while rangofinal<rangoinicio:#esta le indica al usuario que el rango final tiene que ser menor que el rango final
        print("el rango inicial debe ser menor que el final")
        rangoinicio=int(input("ingrese el rango inicial a multiplicar: "))
        rangofinal=int(input("ingrese el rango final: "))

    while tabinicio<tabfinal:# este while es para empezar las operaciones, una vez cumplidas las condicionantes anteriores
        for i in range(tabinicio,tabfinal+1):#recorre el rango ingresado por el usuario

            if i==4:
                print("la tabla {0} no la imprimo, porque no quiero XD".format(i))
                continue
            for j in range(rangoinicio,rangofinal+1):#este multiplica los resultados del anterior "for" por los rangos ingrsados
                if j==5:
                    print("mas de cinco no")
                    break
                resultado=i*j#operacion de las tablas
                #print("multiplicar",i,"x ", j,"es igual a: ",resultado)
                print(chr(27)+"[1;32"+"multiplicar %d x %d es igual a: %d "%(i,j,resultado))
        tabinicio+=1
    opcion=input("desea ejecutar nuevamente el proceso :\n"#aqui se le da la opcion al usuario de continuar o cerrar el programa, dando las opciones de las condicionantes del while principal
                         "ingrese n para salir o s para continuar:\t")
else:#en caso de que el usuario deseara salir, como se cierra el ciclo, se le muestra un mensaje
    print("gracias por jugar")
